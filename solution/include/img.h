#ifndef IMAGE
#define IMAGE


#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>



struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image new_image(const uint64_t width,const uint64_t height);

void clear_image_mem(struct image image);


#endif
