#include "../include/img.h"
#include <stdint.h>
#include <stdio.h>

 enum{
    BMP_SIGNATURE=0x4D42,
    BITMAPINFOHEADER=40,
    PLANES_NUMBER=1,
    BITS_PER_PIXEL=24,
    COMPRESSION_TYPE=0
};


struct __attribute__((packed)) bmp_header;

uint16_t get_bf_type(   struct bmp_header* bmpHeader);

uint32_t get_bi_width( struct bmp_header* bmpHeader);

uint32_t get_bi_height( struct bmp_header* bmpHeader);

uint16_t get_bi_bit_count(struct  bmp_header* bmpHeader);

void set_bf_type( struct bmp_header* bmpHeader, uint16_t value);

void set_bi_width(struct  bmp_header* bmpHeader,uint32_t value);

void set_bi_height(struct bmp_header* bmpHeader,uint32_t value);

void set_bi_bit_count(struct  bmp_header* bmpHeader,uint16_t value);

void set_bf_reserved(struct bmp_header* bmpHeader, uint32_t value);

void set_b_off_bits(struct bmp_header* bmpHeader, uint32_t value);

void set_bi_size(struct bmp_header* bmpHeader, uint32_t value);

void set_bi_planes(struct bmp_header* bmpHeader, uint16_t value);

void set_b_file_size(struct bmp_header* bmpHeader, uint32_t value);


enum bmp_read_status  {
    BMP_READ_SUCCESS = 0,
    INVALID_SIGNATURE,
    INVALID_BITS,
    INVALID_HEADER,
    BIT_NUMBER_ERROR,
    PIXEL_ERROR,
    MEMORY_ERROR,
    READING_ERROR

};


enum  bmp_write_status  {
    BMP_WRITE_SUCCESS=MEMORY_ERROR+1,
    ERROR
};

enum bmp_read_status from_bmp( FILE* in, struct image* img);

enum bmp_write_status to_bmp( FILE* out, struct image const* img);
