#include "../include/transform.h"
#include "../include/img.h"
#include <stddef.h>

struct image transform_rotate( struct image const source ) {
    struct image image = new_image(source.height,source.width);
    if (image.data == NULL) return image; 
    for (size_t k = 0;k<source.width;k++) {
        for (size_t n = 0;n<source.height;n++) {
            image.data[k*source.height+n] = 
            source.data[k+source.height*source.width -source.width -n*source.width];
            
        }
    }
    return image;
}
